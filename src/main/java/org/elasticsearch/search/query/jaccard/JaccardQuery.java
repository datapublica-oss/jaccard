package org.elasticsearch.search.query.jaccard;

import org.apache.lucene.index.IndexReaderContext;
import org.apache.lucene.search.*;
import org.apache.lucene.util.BytesRef;
import org.elasticsearch.search.query.jaccard.scoring.JaccardWeight;

import java.io.IOException;
import java.util.Map;
import java.util.Objects;

/**
 * samuel
 * 22/10/15, 14:11
 */
public class JaccardQuery extends Query {
    private final Map<BytesRef, Long> uniqueTerms;
    private final int querySize;
    private final String field;
    private final String dfField;
    private float boost;
    private float missingTerms;
    private boolean fuzziness;

    public JaccardQuery(Map<BytesRef, Long> uniqueTerms, int querySize, String field, String dfField, float boost, float missingTerms, boolean fuzziness) {
        this.uniqueTerms = uniqueTerms;
        this.querySize = querySize;
        this.field = field;
        this.dfField = dfField;
        this.boost = boost;
        this.missingTerms = missingTerms;
        this.fuzziness = fuzziness;
    }

    @Override
    public String toString(String s) {
        return "jaccard(terms="+uniqueTerms.toString()+", querySize="+querySize+", fuzziness="+fuzziness+")";
    }

    @Override
    public Weight createWeight(IndexSearcher searcher, ScoreMode needsScores, float boost) throws IOException {
        IndexReaderContext context = searcher.getTopReaderContext();
        return new JaccardWeight(this, searcher, context);
    }

    public String getField() {
        return field;
    }

    public String getDfField() {
        return dfField;
    }

    public Map<BytesRef, Long> getUniqueTerms() {
        return uniqueTerms;
    }

    public boolean getFuzziness() {
        return fuzziness;
    }

    public float getMissingTerms() {
        return missingTerms;
    }

    public int getQuerySize() {
        return querySize;
    }

    public float getBoost() {
        return boost;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        JaccardQuery that = (JaccardQuery) o;
        return querySize == that.querySize &&
                Float.compare(that.boost, boost) == 0 &&
                Objects.equals(uniqueTerms, that.uniqueTerms) &&
                Objects.equals(missingTerms, that.missingTerms) &&
                Objects.equals(fuzziness, that.fuzziness) &&
                Objects.equals(field, that.field);
    }

    @Override
    public int hashCode() {
        return Objects.hash(uniqueTerms, querySize, field, boost, missingTerms, fuzziness);
    }

}
