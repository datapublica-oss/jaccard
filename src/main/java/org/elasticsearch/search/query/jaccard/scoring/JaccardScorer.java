package org.elasticsearch.search.query.jaccard.scoring;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.lucene.search.DocIdSetIterator;
import org.apache.lucene.search.Scorer;
import org.apache.lucene.search.Weight;
import org.apache.lucene.util.BytesRef;

import java.io.IOException;

/**
 * samuel
 * 23/10/15, 15:18
 */
public class JaccardScorer extends Scorer {
    public static final Logger log = LogManager.getLogger(JaccardScorer.class);
    private final BytesRef term;
    private final float queryTF;
    private final Scorer postings;
    private final float dfWeight;

    public JaccardScorer(Weight weight, BytesRef term, float queryTF, Scorer postings, int docFreq) {
        super(weight);
        this.term = term;
        this.queryTF = queryTF;
        this.postings = postings;
        this.dfWeight = dfWeight(docFreq);
    }

    public static float dfWeight(int docFreq) {
        /*
            This is a correction meant to soften the impact on the similarity score of having an out-of-vocabulary
            token in the query. This happens for instance when the company name field is not cleaned thoroughly and
            contains an internal ID (real examples from the STN database: "IRISH CLOTHING COMPANY LTD GBR09002"
            or "EUROPE INCOMING/TRADETRAV LTD C693540000000000").
            For tokens that occur really rarely (less than five times), we also cap the IDF to avoid threshold effects.
        */
        if (docFreq == 0) {
            docFreq = 50;
        } else if (docFreq <= 5) {
            docFreq = 5;
        }
        return (float) (1f / (Math.log1p(docFreq)));
    }

    @Override
    public DocIdSetIterator iterator() {
        if (postings == null) {
            return DocIdSetIterator.empty();
        }
        return postings.iterator();
    }

    @Override
    public float getMaxScore(int upTo) throws IOException {
        return dfWeight * Math.min(queryTF, postings.getMaxScore(upTo));
    }

    @Override
    public int docID() {
        if (postings == null) {
            return Integer.MAX_VALUE;
        }
        return postings.docID();
    }

    @Override
    public float score() throws IOException {
        /*
            We apply a sublinear transformation function on the TF to minimize the impact of having a repeated
            company name in the query (for instance, comparing "Sidetrade Sidetrade" to "Sidetrade".
            Ditto for the norm() method below.
         */
        return dfWeight * Math.min((float) Math.pow(queryTF, 1 / 4.0), approxFreq());
    }

    public float approxFreq() throws IOException {
        return postings.score();
    }

    public float norm() throws IOException {
        return dfWeight * Math.max((float) Math.pow(queryTF, 1 / 4.0), approxFreq());
    }

    public BytesRef getTerm() {
        return term;
    }

    public float getQueryTF() {
        return queryTF;
    }

    public float getDfWeight() {
        return dfWeight;
    }
}
