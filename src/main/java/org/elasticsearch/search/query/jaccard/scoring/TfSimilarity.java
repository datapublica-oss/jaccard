//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package org.elasticsearch.search.query.jaccard.scoring;

import org.apache.lucene.index.FieldInvertState;
import org.apache.lucene.search.CollectionStatistics;
import org.apache.lucene.search.Explanation;
import org.apache.lucene.search.TermStatistics;
import org.apache.lucene.search.similarities.BM25Similarity;
import org.apache.lucene.search.similarities.Similarity;

public class TfSimilarity extends Similarity {
    private static final Similarity BM25_SIM = new BM25Similarity();

    public TfSimilarity() {
    }

    public long computeNorm(FieldInvertState state) {
        return BM25_SIM.computeNorm(state);
    }

    public SimScorer scorer(float boost, CollectionStatistics collectionStats, TermStatistics... termStats) {
        return new SimScorer() {
            @Override
            public float score(float freq, long norm) {
                return boost * freq;
            }

            public Explanation explain(Explanation freq, long norm) {
                return Explanation.match(boost * freq.getValue().floatValue(), "score(" + this.getClass().getSimpleName() + "), computed from:", Explanation.match(boost, "query boost"), freq);
            }
        };
    }
}
