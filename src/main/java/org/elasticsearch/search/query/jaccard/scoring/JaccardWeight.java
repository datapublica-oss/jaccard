package org.elasticsearch.search.query.jaccard.scoring;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.lucene.index.*;
import org.apache.lucene.search.*;
import org.apache.lucene.util.BytesRef;
import org.apache.lucene.util.SmallFloat;
import org.elasticsearch.search.query.jaccard.JaccardQuery;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

import static org.elasticsearch.search.query.jaccard.scoring.JaccardScorer.dfWeight;

/**
 * samuel
 * 22/10/15, 14:55
 */
public class JaccardWeight extends Weight {
    public static final Logger log = LogManager.getLogger(JaccardWeight.class);

    public JaccardQuery query;
    private final Map<BytesRef, Weight> weights = new HashMap<>();
    private final Map<BytesRef, Integer> docFreq = new HashMap<>();

    public JaccardWeight(JaccardQuery query, IndexSearcher searcher, IndexReaderContext context) throws IOException {
        super(query);
        this.query = query;
//        System.out.println("Creating weight");
        searcher.setSimilarity(new TfSimilarity());
        buildWeights(context, searcher, query);
        buildDocFreq(context, query);
    }

    @Override
    public Explanation explain(LeafReaderContext context, int doc) throws IOException {
        final List<JaccardScorer> scorers = getScorers(context);
        List<Explanation> explanations = new ArrayList<>(scorers.size());
        final String histAsString = query.getUniqueTerms().entrySet().stream().collect(Collectors.toMap(it -> it.getKey().utf8ToString(), Map.Entry::getValue)).toString();

        double score = 0.0D;
        double norm = 0.0D;

        final int querySize = query.getQuerySize();
        final double totalTermWeight = scorers.stream().mapToDouble(it -> it.getDfWeight() * it.getQueryTF()).sum();
        final NumericDocValues normValues = context.reader().getNormValues(query.getField());

        float missingDocTerms = querySize;
        if (normValues != null && normValues.advanceExact(doc)) {
            missingDocTerms = decodeNormValue(normValues.longValue());
        }
        double missingWeightedQueryTerms = totalTermWeight;
        for (JaccardScorer scorer : scorers) {
            if (scorer.iterator().advance(doc) != doc) {
                explanations.add(
                        Explanation.match(
                                0, "term="+scorer.getTerm().utf8ToString()+" min of (queryTF, docTF)",
                                Explanation.match(scorer.getQueryTF(), "queryTF"),
                                Explanation.match(0, "docTF"),
                                Explanation.match(scorer.getDfWeight(), "df weight")
                        )
                );
                continue;
            }
            missingWeightedQueryTerms -= scorer.getDfWeight() * scorer.getQueryTF();
            missingDocTerms -= scorer.approxFreq();
            explanations.add(
                Explanation.match(
                    scorer.score(), "term="+scorer.getTerm().utf8ToString()+" min of (queryTF, docTF)",
                    Explanation.match(scorer.getQueryTF(), "queryTF"),
                    Explanation.match(scorer.approxFreq(), "docTF"),
                    Explanation.match(scorer.getDfWeight(), "df weight")
                )
            );
            score += ((double) scorer.score());
            norm += (double) scorer.norm();
        }

        if (score == 0) {
            return Explanation.match(0, "jaccard(field='"+query.getField()+"',terms="+histAsString+")",
                    Explanation.match((float) 0, "terms"));
        }

        final double missingDocPenalty = Math.sqrt(missingDocTerms) * query.getMissingTerms();
        return Explanation.match(
                query.getBoost() *  (float)(score / (norm + missingDocPenalty + missingWeightedQueryTerms)), "jaccard(field='"+query.getField()+"',terms="+histAsString+")",
                Explanation.match((float) missingDocPenalty, "missingDocPenalty = sqrt(missingDocTerm) * DOC_PENALTY", Explanation.match(missingDocTerms, "missingDocTerms")),
                Explanation.match(query.getQuerySize(), "queryTotalTerms"),
                Explanation.match((float) missingWeightedQueryTerms, "missingWeightedQueryTerms"),
                Explanation.match((float) norm, "sum of max(queryTF, docTF)"),
                Explanation.match(query.getBoost(), "boost"),
                Explanation.match((float) score, "terms", explanations)
        );
    }

    @Override
    public void extractTerms(Set<Term> set) {
    }

    @Override
    public Scorer scorer(LeafReaderContext context) throws IOException {
        long t = System.currentTimeMillis();
        final List<JaccardScorer> allScores = getScorers(context);
        t = System.currentTimeMillis() - t;
//        System.out.println("Time to create the scorers: "+t);
        if (allScores.size() == 0) {
            return null;
        }
        final int querySize = query.getQuerySize();
        final double totalTermWeight = query.getUniqueTerms().entrySet().stream().mapToDouble(it -> dfWeight(docFreq.get(it.getKey())) * it.getValue()).sum();
        final NumericDocValues normValues = context.reader().getNormValues(query.getField());
        if (allScores.size() == 1) {
            final JaccardScorer scorer = allScores.get(0);
            return new Scorer(this) {

                @Override
                public int docID() {
                    return scorer.docID();
                }

                @Override
                public float score() throws IOException {
                    normValues.advanceExact(docID());
                    final float docSize = decodeNormValue(normValues.longValue());
                    double missingWeightedQueryTerms = totalTermWeight - (scorer.getDfWeight() * scorer.getQueryTF());

                    return query.getBoost() * (float)(scorer.score() / (scorer.norm() + (Math.sqrt(docSize - scorer.approxFreq()) * query.getMissingTerms()) + missingWeightedQueryTerms));
                }

                @Override
                public DocIdSetIterator iterator() {
                    return scorer.iterator();
                }

                @Override
                public float getMaxScore(int upTo) throws IOException {
                    return query.getBoost();
                }
            };
        }
        return new DisjunctionScorer(this, allScores, true) {
            @Override
            public float getMaxScore(int upTo) throws IOException {
                return query.getBoost();
            }

            @Override
            protected float score(DisiWrapper topList) throws IOException {
                double score = 0.0D;
                double norm = 0.0D;

                float missingDocTerms = querySize;
                if (normValues != null) {
                    normValues.advanceExact(topList.doc);
                    missingDocTerms = decodeNormValue(normValues.longValue());
                }
                double missingWeightedQueryTerms = totalTermWeight;
                for(DisiWrapper w = topList; w != null; w = w.next) {
                    final JaccardScorer scorer = (JaccardScorer) w.scorer;
                    score += scorer.score();
                    norm += scorer.norm();
                    missingDocTerms -= scorer.approxFreq();
                    missingWeightedQueryTerms -= scorer.getDfWeight() * scorer.getQueryTF();
                }
                return query.getBoost() * (float)(score / (norm + (Math.sqrt(missingDocTerms) * query.getMissingTerms()) + missingWeightedQueryTerms));
            }
        };
    }

    private void buildWeights(IndexReaderContext context, IndexSearcher searcher, JaccardQuery query) throws IOException {
        for (BytesRef bytes : query.getUniqueTerms().keySet()) {
            Term term = new Term(query.getField(), bytes);
            int len = term.text().length();
            int maxEdits = len > 2 ? (len > 5 ? 2 : 1) : 0;
            Weight weight;
            if (query.getFuzziness()) {
                /* These LOOK like magic values, but they are in fact default values when building a MatchQuery internally.
                   With the exception of `Math.ceil(len / 2.0)`, where we force a prefix match on the first half of the term.
                   This is done for optimization reasons (the number of expansions would be very big otherwise) BUT
                   it has important impacts since it means that we cannot have typos on the first half of the term...  */
                FuzzyQuery fuzzyQuery = new FuzzyQuery(term, maxEdits, (int) Math.ceil(len / 2.0), 50, false);

                Query rewrite = fuzzyQuery.rewrite(context.reader()).rewrite(context.reader());
                weight = rewrite.createWeight(searcher, ScoreMode.COMPLETE, 1.0f);
            } else {
                TermQuery termQuery = new TermQuery(term);
                weight = termQuery.createWeight(searcher, ScoreMode.COMPLETE, 1.0f);
            }
            weights.put(bytes, weight);
        }
    }

    private void buildDocFreq(IndexReaderContext context, JaccardQuery query) throws IOException {
        for (BytesRef bytes : query.getUniqueTerms().keySet()) {
            final Term term = new Term(this.query.getDfField(), bytes);
            TermStates termContext = TermStates.build(context, term, true);
            docFreq.put(bytes, termContext.docFreq());
        }
    }

    private List<JaccardScorer> getScorers(LeafReaderContext context) throws IOException {
        List<JaccardScorer> scorers = new ArrayList<>(query.getUniqueTerms().size());
        for (Map.Entry<BytesRef, Long> entry : query.getUniqueTerms().entrySet()) {
            Weight termWeight = this.weights.get(entry.getKey());
            Scorer termScorer = termWeight.scorer(context);
            if (termScorer == null)
                continue;
            Integer docFreq = this.docFreq.get(entry.getKey());
            JaccardScorer jaccardScorer = new JaccardScorer(this, entry.getKey(), entry.getValue(), termScorer, docFreq);
            scorers.add(jaccardScorer);
        }
        return scorers;
    }

    protected static float decodeNormValue(long b) {
        return NORM_TABLE[((byte) ((int) b)) & 255];
    }

    private static final int[] NORM_TABLE = new int[256];
    static {
        for(int i = 255; i >= 1; --i) {
            NORM_TABLE[i] = SmallFloat.byte4ToInt((byte)i);
        }
    }
    /**/
    protected static byte encodeNormValue(int fieldLength) {
        return SmallFloat.intToByte4(fieldLength);
    }

    public static void main(String[] args) {
        for(int i = 0 ; i < 255 ; i++) {
            System.out.println(i+": "+encodeNormValue(i)+ " / "+decodeNormValue(encodeNormValue(i)));
        }
    }

    @Override
    public boolean isCacheable(LeafReaderContext ctx) {
        return false;
    }
}
