package org.elasticsearch.search.query.jaccard;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.TermToBytesRefAttribute;
import org.apache.lucene.search.Query;
import org.apache.lucene.util.BytesRef;
import org.elasticsearch.ElasticsearchException;
import org.elasticsearch.common.io.stream.StreamInput;
import org.elasticsearch.common.io.stream.StreamOutput;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.index.mapper.MappedFieldType;
import org.elasticsearch.index.query.AbstractQueryBuilder;
import org.elasticsearch.index.query.QueryShardContext;

import java.io.IOException;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * samuel
 * 19/11/15, 15:57
 */
public class JaccardQueryBuilder extends AbstractQueryBuilder {
    private static final float MISSING_TERM_PENALTY = 0.05f; // JaccardScorer.dfWeight(5000);
    private String query;
    private String field;
    private String dfField;
    private float missingTerms = MISSING_TERM_PENALTY;
    private boolean fuzziness = true;

    public JaccardQueryBuilder() {
    }

    public JaccardQueryBuilder(StreamInput in) throws IOException {
        super(in);
        this.query = in.readString();
        this.field = in.readString();
        this.dfField = in.readOptionalString();
        this.missingTerms = in.readFloat();
        this.fuzziness = in.readBoolean();
    }

    @Override
    public String getWriteableName() {
        return JaccardQueryParser.NAME;
    }

    @Override
    protected void doWriteTo(StreamOutput out) throws IOException {
        out.writeString(query);
        out.writeString(field);
        out.writeOptionalString(dfField);
        out.writeFloat(missingTerms);
        out.writeBoolean(fuzziness);
    }

    /**
     * Sets the query to filter & sort
     */
    public JaccardQueryBuilder query(String query) {
        this.query = query;
        return this;
    }

    /**
     * Sets the routing for the doc to lookup
     */
    public JaccardQueryBuilder field(String field) {
        this.field = field;
        return this;
    }

    public JaccardQueryBuilder dfField(String dfField) {
        this.dfField = dfField;
        return this;
    }

    public JaccardQueryBuilder missingTerms(float missingTerms) {
        this.missingTerms = missingTerms;
        return this;
    }

    public JaccardQueryBuilder fuzziness(boolean fuzziness) {
        this.fuzziness = fuzziness;
        return this;
    }

    public void validate(Function<String, ElasticsearchException> exceptionProvider) throws IOException {
        if (field == null) {
            throw exceptionProvider.apply("[jaccard] field is absent");
        }
        if (query == null) {
            throw exceptionProvider.apply("[jaccard] query is absent");
        }
    }

    @Override
    protected boolean doEquals(AbstractQueryBuilder o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        JaccardQueryBuilder that = (JaccardQueryBuilder) o;
        return Objects.equals(query, that.query) &&
                Objects.equals(field, that.field) &&
                Objects.equals(missingTerms, that.missingTerms) &&
                Objects.equals(fuzziness, that.fuzziness) &&
                Objects.equals(dfField, that.dfField);
    }

    @Override
    protected int doHashCode() {
        return Objects.hash(super.hashCode(), query, field, missingTerms, dfField, fuzziness);
    }

    @Override
    protected void doXContent(XContentBuilder builder, Params params) throws IOException {
        builder.startObject(JaccardQueryParser.NAME);

        builder.field("query", query);
        builder.field("field", field);
        builder.field("dfField", dfField);
        builder.field("missingTerms", missingTerms);
        builder.field("fuzziness", fuzziness);

        printBoostAndQueryName(builder);
        builder.endObject();
    }

    @Override
    protected Query doToQuery(QueryShardContext context) throws IOException {
        MappedFieldType fieldType = context.getMapperService().fullName(field);
        if (fieldType == null) {
            throw new IllegalArgumentException("[jaccard] the field " + field + " does not exists locally");
        }
        final String dfField = this.dfField == null ? field : this.dfField;
        MappedFieldType dfFieldType = context.getMapperService().fullName(dfField);
        if (dfFieldType == null) {
            throw new IllegalArgumentException("[jaccard] the dfField " + dfField + " does not exists locally");
        }

        final Analyzer analyzer = fieldType.searchAnalyzer().analyzer();

        List<BytesRef> terms = new LinkedList<>();
        try (final TokenStream stream = analyzer.tokenStream(field, query)) {
            TermToBytesRefAttribute termAtt = stream.getAttribute(TermToBytesRefAttribute.class);
            stream.reset();

            while (stream.incrementToken()) {
                final BytesRef bytesRef = BytesRef.deepCopyOf(termAtt.getBytesRef());
                terms.add(bytesRef);

//                System.out.println("Adding term:" + bytesRef.utf8ToString());
            }
        }
//        System.out.println("Total terms:" + terms.size());
        int querySize = terms.size();
        final Map<BytesRef, Long> uniqueTerms = terms.stream().collect(Collectors.groupingBy(it -> it, Collectors.counting()));

//        System.out.println("Unique terms:" + terms.size());

        // filter to only keep elements referenced in the lookup document
//        List<BoostQuery> filter = uniqueTerms.entrySet().stream().map(it -> new BoostQuery(fieldType.termQuery(it.getKey(), context), it.getValue().floatValue()))
//                .forEach();

        return new JaccardQuery(uniqueTerms, querySize, fieldType.name(), dfFieldType.name(), boost, missingTerms, fuzziness);
    }
}
