/*
 * Licensed to Elasticsearch under one or more contributor
 * license agreements. See the NOTICE file distributed with
 * this work for additional information regarding copyright
 * ownership. Elasticsearch licenses this file to you under
 * the Apache License, Version 2.0 (the "License"); you may
 * not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package org.elasticsearch.search.query.jaccard;

import org.elasticsearch.common.ParsingException;
import org.elasticsearch.common.xcontent.XContentParser;
import org.elasticsearch.index.query.AbstractQueryBuilder;

import java.io.IOException;


/**
 * <pre>
 * "jaccard" : {
 *  "query": "bla bla bla"
 *  "field": "myfield"
 *  "boost": 0.1
 * }
 * </pre>
 */
public class JaccardQueryParser {
    public static final String NAME = "jaccard";

    public static JaccardQueryBuilder fromXContent(XContentParser parser) throws IOException {
        String currentFieldName = null;
        JaccardQueryBuilder builder = new JaccardQueryBuilder();

        XContentParser.Token token;

        while ((token = parser.nextToken()) != XContentParser.Token.END_OBJECT) {
            if (token == XContentParser.Token.FIELD_NAME) {
                currentFieldName = parser.currentName();
            } else if (token == XContentParser.Token.START_OBJECT) {
            } else if (token.isValue() && currentFieldName != null) {
                if (false) {
                } else if ("query".equals(currentFieldName)) {
                    builder.query(parser.text());
                } else if ("field".equals(currentFieldName)) {
                    builder.field(parser.text());
                } else if ("dfField".equals(currentFieldName)) {
                    builder.dfField(parser.text());
                } else if ("missingTerms".equals(currentFieldName)) {
                    builder.missingTerms(parser.floatValue());
                } else if ("fuzziness".equals(currentFieldName)) {
                    builder.fuzziness(parser.booleanValue());
                } else if (AbstractQueryBuilder.NAME_FIELD.match(currentFieldName, parser.getDeprecationHandler())) {
                    builder.queryName(parser.text());
                } else if (AbstractQueryBuilder.BOOST_FIELD.match(currentFieldName, parser.getDeprecationHandler())) {
                    builder.boost(parser.floatValue());
                } else {
                    throw new ParsingException(parser.getTokenLocation(), "[sort_by_doc] query does not support [" + currentFieldName + "] within lookup element");
                }
            }
        }

        builder.validate(str -> new ParsingException(parser.getTokenLocation(), str));
        return builder;
    }

}