Jaccard Operator for Elasticsearch
===================================

### Description

When you perform a standard text query on a field, you will get a match and a scoring based on the terms and their BM25 scores.
However, the given score for this "match" is unbounded and is not comparable across queries.

The jaccard plugin provides a new operator that will give you a matching score that is similar (but not equal to) a generalized jaccard similarity.
Due to constraints of the lucene index structure, some of its characteristics are altered in order to provide a very fast operator.
It can be used on reconciliation projects.

### Operator Definition

#### Jaccard index rewrite

Let $`q=(q_1,...,q_n)`$ and $`d=(d_1,...,d_n)`$ be the terms of the query ($`q`$) and of the field to match in the document ($`d`$), the value of each component is the term frequency.

The traditional jaccard index is:
```math
\frac{\sum_i \min(q_i, d_i)}{\sum_i \max(q_i, d_i)}
```

Because we will try to find the terms of $`q`$ in the index, we can select the indices where $`q_i`$ is not null, let's call it $`I_q`$. The jaccard index can be rewriten as
```math
\frac{\sum_{i\in I_q} \min(q_i, d_i)}{\sum_{i\in I_q} \max(q_i, d_i) + \sum_{i\not\in I_q} d_i}
```

We do not have access to the direct value of the missing terms. However, we have access to an estimate of $`D = \sum_i d_i`$ (in the BM25 stored norm).
```math
\frac{\sum_{i\in I_q} \min(q_i, d_i)}{\sum_{i\in I_q} \max(q_i, d_i) + (D - \sum_{i\in I_q} d_i)}
```
 
#### Adjusting penalties

In order to better handle the fact that some terms are more important than other, we weight the sum by the idf factor of the term $`\mathrm{idf}_i`$.
Unfortunately, we only have access to those data for $`i\in I_q`$. Others will have a fixed weight of $`\mathrm{mtp}`$ (missing term penalty).

```math
\frac{\sum_{i\in I_q} \mathrm{idf}_i.\min(q_i, d_i)}{\sum_{i\in I_q} \mathrm{idf}_i.\max(q_i, d_i) + \mathrm{mtp}.(D - \sum_{i\in I_q} d_i)}
```

Finally, in order to avoid a too large penalty from missing terms (e.g. matching `Sidetrade` against `The large foundation and editorial group of Sidetrade in Boulogne`). A square root is applied to the missing terms:
```math
\frac{\sum_{i\in I_q} \mathrm{idf}_i.\min(q_i, d_i)}{\sum_{i\in I_q} \mathrm{idf}_i.\max(q_i, d_i) + \mathrm{mtp}.\sqrt{D - \sum_{i\in I_q} d_i}}
```

#### Fuzzy search
The fuzzy search mode can be applied. This will search for more terms in the index and apply a penalty on the selected term frequency (same as the fuzzy operator).
If the mode is applied, here are the parameters:
* `fuzzy = AUTO`, the number of edits depends on the size of the term _(default of the fuzzy operator, it's 2 edits for terms longer than 5, 1 for terms longer than 2, 0 else)_.
* `prefix = ceil(length/2)`, number of characters that can't be edited at the start of a word. Avoids useless expansions.   
* `max_expansions = 50`, number of possibles terms to expand to _(default of the fuzzy operator)_
* `transpositions = false`, disallows transposition edits

Usage
==========

##### Recommended Versions

ElasticSearch version 5.2.0: 0.0.1.1

ElasticSearch version 7.3.2: 0.0.2.1

##### Parameters
* query - The text query
* field - The field on which the query is tested
* dfField - The field from which $`df`$ are gathered
* mtp - The missing term penalty, default 0.05
* fuzziness - Boolean, is the fuzziness search applied
* boost - boost value
